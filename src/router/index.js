import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/views/HelloWorld'
import ActionSheetTest from '@/views/ActionSheetTest'
import ButtonTest from '@/views/ButtonTest'
import CellTest from '@/views/CellTest'
import CheckboxTest from '@/views/CheckboxTest'
import DatetimePickerTest from '@/views/DatetimePickerTest'
import DialogTest from '@/views/DialogTest'
import IconTest from '@/views/IconTest'
import InputTest from '@/views/InputTest'
import KeyboardTest from '@/views/KeyboardTest'
import LoadingTest from '@/views/LoadingTest'
import NavbarTest from '@/views/NavbarTest'
import CodeBoxTest from '@/views/CodeBoxTest'
import PickerTest from '@/views/PickerTest'
import PopupTest from '@/views/PopupTest'
import RadioTest from '@/views/RadioTest'
import RateTest from '@/views/RateTest'
import RowColTest from '@/views/RowColTest'
import SearchTest from '@/views/SearchTest'
import StepperTest from '@/views/StepperTest'
import ToastTest from '@/views/ToastTest'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/action-sheet',
      name: 'ActionSheetTest',
      component: ActionSheetTest
    },
    {
      path: '/button',
      name: 'ButtonTest',
      component: ButtonTest
    },
    {
      path: '/cell',
      name: 'CellTest',
      component: CellTest
    },
    {
      path: '/checkbox',
      name: 'CheckboxTest',
      component: CheckboxTest
    },
    {
      path: '/datetime-picker',
      name: 'DatetimePickerTest',
      component: DatetimePickerTest
    },
    {
      path: '/dialog',
      name: 'DialogTest',
      component: DialogTest
    },
    {
      path: '/icon',
      name: 'IconTest',
      component: IconTest
    },
    {
      path: '/input',
      name: 'InputTest',
      component: InputTest
    },
    {
      path: '/keyboard',
      name: 'KeyboardTest',
      component: KeyboardTest
    },
    {
      path: '/loading',
      name: 'LoadingTest',
      component: LoadingTest
    },
    {
      path: '/navbar',
      name: 'NavbarTest',
      component: NavbarTest
    },
    {
      path: '/code-box',
      name: 'CodeBoxTest',
      component: CodeBoxTest
    },
    {
      path: '/picker',
      name: 'PickerTest',
      component: PickerTest
    },
    {
      path: '/popup',
      name: 'PopupTest',
      component: PopupTest
    },
    {
      path: '/radio',
      name: 'RadioTest',
      component: RadioTest
    },
    {
      path: '/rate',
      name: 'RateTest',
      component: RateTest
    },
    {
      path: '/rowcol',
      name: 'RowColTest',
      component: RowColTest
    },
    {
      path: '/search',
      name: 'SearchTest',
      component: SearchTest
    },
    {
      path: '/stepper',
      name: 'StepperTest',
      component: StepperTest
    },
    {
      path: '/toast',
      name: 'ToastTest',
      component: ToastTest
    }
  ]
})
