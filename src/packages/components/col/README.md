### 示例
```html
<pm-row>
  <pm-col span="8"><div class="content">span_8</div></pm-col>
  <pm-col span="8"><div class="content">span_8</div></pm-col>
  <pm-col span="8"><div class="content">span_8</div></pm-col>
</pm-row>
```
### 属性方法
#### props
| 参数 | 类型 | 说明 | 可选参数 | 默认值 |
| --- | --- | --- | --- | ---|
| span| Number \| String | 占比 | - | - |
| offset | Number \| String | 偏移 | - | - |
#### events
无
