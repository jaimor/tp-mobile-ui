import ActionSheet from './action-sheet/index.js';
import Button from './button/index.js';
import Cell from './cell/index.js';
import CellGroup from './cell-group/index.js';
import Checkbox from './checkbox/index.js';
import CheckboxGroup from './checkbox-group/index.js';
import Col from './col/index.js';
import DatetimePicker from './datetime-picker/index.js';
import Dialog from './dialog/index.js';
import Icon from './icon/index.js';
import Input from './input/index.js';
import Keyboard from './keyboard/index.js';
import Loading from './loading/index.js';
import Navbar from './navbar/index.js';
import CodeBox from './code-box/index.js';
import Picker from './picker/index.js';
import Popup from './popup/index.js';
import Radio from './radio/index.js';
import RadioGroup from './radio-group/index.js';
import Rate from './rate/index.js';
import Row from './row/index.js';
import Search from './search/index.js';
import Stepper from './stepper/index.js';
import Textarea from './textarea/index.js';
import Toast from './toast/index.js';

const components = [
  ActionSheet,
  Button,
  Cell,
  CellGroup,
  Checkbox,
  CheckboxGroup,
  Col,
  DatetimePicker,
  Icon,
  Input,
  Keyboard,
  Loading,
  Navbar,
  CodeBox,
  Picker,
  Popup,
  Radio,
  RadioGroup,
  Rate,
  Row,
  Search,
  Stepper,
  Textarea
];

const install = Vue => {
  components.forEach(component => {
    if (typeof component.install === "function") component.install(Vue);
    else Vue.component(component.name, component);
  });
  Vue.prototype.$toast = Toast;
  Vue.prototype.$dialog = Dialog;
};

export default {
  install
}
