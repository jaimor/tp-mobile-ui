### 示例
```html
<pm-datetime-picker title="请选择日期" v-model="date3" type="datetime" @on-done="onDone"></pm-datetime-picker>
```
### 属性方法
#### props
| 参数 | 类型 | 说明 | 可选参数 | 默认值 |
| --- | --- | --- | --- | ---|
| title| String | 标题 | - | - |
| years | Array | 设置年的范围 | - | [当前年-10， 当前年+10] |
| type | Array | 显示的格式类别 | `year-month-day`, `year-month-day hh:mm`, `year`, `month`, `year-month`, `hh:mm` | - |
#### events
| 事件名称 | 说明 | 回调参数 | 
| --- | --- | --- |
| on-done | 点击完成事件 | - |
| on-cancel | 点击取消事件 | - |
