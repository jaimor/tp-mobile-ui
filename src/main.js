import Vue from 'vue'
import App from './App.vue'
import router from './router'

import './packages/style/index.less'
import TpMobileUI from './packages/components/index.js'
Vue.use(TpMobileUI);

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
