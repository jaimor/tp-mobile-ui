# TpMobileUI
一个基于Vue的手机端UI组件库

### 主要组件
- ActionSheet
- Button
- Cell/Cell-Group
- Checkbox/CheckboxGroup
- CodeBox
- Row/Col
- DatetimePicker
- Dialog
- Icon
- Input
- Keyboard
- Loading
- Navbar
- Picker
- Popup
- Radio/RadioGroup
- Rate
- Search
- Stepper
- Textarea
- Toast

### 命令
```
npm install
npm run serve
```

### 演示地址
[跳转演示地址](http://underline.gitee.io/tp-mobile-ui)
